#pragma once
#include <glm/glm.hpp>

using namespace std;
using namespace glm;

class ParticleSystem;
class Emitter
{
public:
	void Update(float deltaTime, ParticleSystem *ps);

	Emitter(vec3 position, vec3 orientation, float particlesPerSecond, 
		float strength);

private:
	void GenerateParticle(ParticleSystem *ps);

	vec3 position;
	vec3 orientation;
	float partsPerSec;
	float strength;

	float particlesToGenerate;
};