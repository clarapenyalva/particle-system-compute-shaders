#include "Emitter.h"
#include "ParticleSystem.h"
#include "Utils.h"

void Emitter::Update(float deltaTime, ParticleSystem *ps)
{
	//Comprobar cu�ntas part�culas se tienen que generar y generarlas.
	//Como la generaci�n de part�culas deber�a ser m�s o menos exacta, 
	//	si el deltatime no permite generar una, deber�a acumularse la fracci�n
	//	de part�cula generada hasta llegar a un entero que si pudiera generarse, 
	//	rest�ndose del contador.

	particlesToGenerate += deltaTime * partsPerSec;

	if (particlesToGenerate >= 1.0f) 
	{
		int generableAmount = floor(particlesToGenerate);

		for (size_t i = 0; i < generableAmount; i++)		
			GenerateParticle(ps);
		
		particlesToGenerate -= generableAmount;
	}
}

Emitter::Emitter(vec3 position, vec3 orientation, 
	float particlesPerSecond, float strength)
{
	this->position = position;
	this->orientation = orientation;
	this->partsPerSec = particlesPerSecond;
	this->strength = strength;

	particlesToGenerate = 0.0f;
}

void Emitter::GenerateParticle(ParticleSystem *ps)
{
	ps->AddParticle(
		ranf(0.5f, 4.0f),
		vec4(position + ranv3(-0.1f, 0.1f), 1.0), //pos
		ranv4(1.0f), //color
		vec4(strength * normalize(orientation +  ranv3(-0.1f, 0.1f)) , 0.0f),//spd with spread
		ranf(2.0f, 5.0f));//lifetime
}
