#include "ParticleSystem.h"
#include <GL/glew.h>
#include <GL/glut.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include "Emitter.h"
#include "Utils.h"

void ParticleSystem::InitializeEmitters(unsigned int emitterNumber, float pps, float strenght)
{
	//float pps = 1/(deltaTime*0.8f);
	this->emitters = vector<Emitter>();
	emitters.reserve(emitterNumber);
	vec3 chosenPos;
	for (size_t i = 0; i < emitterNumber; i++)
	{
		chosenPos = ranv3();
		
		emitters.push_back(Emitter(
			chosenPos,
			-chosenPos, // apuntando al centro (vector0 - posicion)
			pps,
			strenght
		));
	}
}

void ParticleSystem::InitializeEmitters(unsigned int emitterNumber)
{
	this->emitters = vector<Emitter>();
	emitters.reserve(emitterNumber);
}

void ParticleSystem::AddEmitter(vec3 position, vec3 direction, float pps, float strenght)
{
	this->emitters.push_back(Emitter(
		position,
		direction,
		pps,
		strenght
	));	
}

void ParticleSystem::InitializeAttractors(unsigned int attractorNumber, GLuint computeProgramID)
{
	this->attractorNumber = attractorNumber;
	//por ahora las fuerzas de los atractores se deciden aqu�
	attractors = new float[attractorNumber * 4];
	attractorsStrengths = new float[attractorNumber];
}


void ParticleSystem::AddAttractor(unsigned int attractorId, vec4 position, float strength)
{
	attractors[4 * attractorId] = position.x;
	attractors[4 * attractorId + 1] = position.y;
	attractors[4 * attractorId + 2] = position.z;
	attractors[4 * attractorId + 3] = position.w;

	attractorsStrengths[attractorId] = strength;
}

void ParticleSystem::BindAttractors(GLuint computeProgramID)
{
	//Attractors
	glGenBuffers(1, &attSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, attSSbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(float) * attractorNumber, attractors, GL_STATIC_DRAW);

	glGenBuffers(1, &attStrSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, attStrSSbo);
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(float) * attractorNumber, attractorsStrengths, GL_STATIC_DRAW);

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, attSSbo);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 6, attStrSSbo);

	attractorNumberU = glGetUniformLocation(computeProgramID, "attractorNumber");
	glUniform1i(attractorNumberU, this->attractorNumber);
}

void ParticleSystem::InitializeParticles(unsigned int partsAmount)
{
	points = new float[maxPartsAmount * 4];
	colors = new float[maxPartsAmount * 4];
	velocs = new float[maxPartsAmount * 4];
	masses = new float[maxPartsAmount];
	lifeTimes = new float[maxPartsAmount];
	iniLifeTimes = new float[maxPartsAmount];

	for (int i = 0; i < maxPartsAmount; i++)
	{
		if (i < partsAmount)
		{
			masses[i] = mass + ranf(-0.5f, 0.5f);
			lifeTimes[i] = ranf(1.0f, 4.0f);
			iniLifeTimes[i] = lifeTimes[i];

			colors[4 * i] = ranf();
			colors[4 * i + 1] = ranf();
			colors[4 * i + 2] = ranf();
			colors[4 * i + 3] = 1.0f;

			velocs[4 * i] = 0.0f;
			velocs[4 * i + 1] = 0.0f;
			velocs[4 * i + 2] = 0.0f;
			velocs[4 * i + 3] = 0.0f;

			points[4 * i] = ranf(-1.0f, 1.0f);
			points[4 * i + 1] = ranf(0.0f, 1.0f);
			points[4 * i + 2] = ranf(-1.0f, 1.0f);
			points[4 * i + 3] = 1.0f;
		}
		else
		{
			//As lifetime is 0, all remaining data is not relevant
			lifeTimes[i] = 0.0f;
			iniLifeTimes[i] = 0.0f;
		}
	}
}

void ParticleSystem::AddParticle(
	float mass, vec4 position,
	vec4 color, vec4 veloc, float startingLifeTime)
{
	if (this->currentPartsAmount >= maxPartsAmount) return;

	masses[currentPartsAmount] = mass;
	lifeTimes[currentPartsAmount] = startingLifeTime;
	iniLifeTimes[currentPartsAmount] = startingLifeTime;

	velocs[4 * currentPartsAmount] = veloc.x;
	velocs[4 * currentPartsAmount + 1] = veloc.y;
	velocs[4 * currentPartsAmount + 2] = veloc.z;
	velocs[4 * currentPartsAmount + 3] = veloc.w;

	colors[4 * currentPartsAmount] = color.r;
	colors[4 * currentPartsAmount + 1] = color.g;
	colors[4 * currentPartsAmount + 2] = color.b;
	colors[4 * currentPartsAmount + 3] = color.a;

	points[4 * currentPartsAmount] = position.x;
	points[4 * currentPartsAmount + 1] = position.y;
	points[4 * currentPartsAmount + 2] = position.z;
	points[4 * currentPartsAmount + 3] = position.w;

	this->currentPartsAmount++;
}

void ParticleSystem::RemoveParticle(unsigned int index)
{
	if (index >= currentPartsAmount) return;
	int maxInd = currentPartsAmount - 1;

	if (maxInd != index) 
	{
		masses[index] = masses[maxInd];
		lifeTimes[index] = lifeTimes[maxInd];
		iniLifeTimes[index] = iniLifeTimes[maxInd];

		for (size_t i = 0; i < 4; i++)
		{
			points[4 * index + i] = points[4 * maxInd + i];
			colors[4 * index + i] = colors[4 * maxInd + i];
			velocs[4 * index + i] = velocs[4 * maxInd + i];
		}
	}
	currentPartsAmount--;
}

ParticleSystem::ParticleSystem
(unsigned int maxPartsAmount, unsigned int partsAmount,
	float deltaTime, vec4 externalForces, vec4 sphereData,
	vec3 minBoxData, vec3 maxBoxData,
	float restitution,
	float mass,
	GLuint graphicProgramID,
	GLuint computeProgramID
)
{
	this->mass = mass;
	this->currentPartsAmount = partsAmount;
	this->maxPartsAmount = maxPartsAmount;
	this->deltaTime = deltaTime;
	this->externalForces = externalForces;
	this->sphereData = sphereData;	
	this->restitution = restitution;

	this->minBoxData = minBoxData;
	this->maxBoxData = maxBoxData;

	InitializeParticles(partsAmount);

	/*En el caso de algunos arrays, necesitamos que los datos est�n sincronizados
		durante toda la ejecuci�n del programa.
	*/
	glGenBuffers(1, &posSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, posSSbo);
	glBufferStorage(GL_SHADER_STORAGE_BUFFER, 4 * sizeof(float) * maxPartsAmount, points,
		GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT);
	points = (float*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, 4 * sizeof(float) * maxPartsAmount,
		GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT);

	glGenBuffers(1, &velSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, velSSbo);
	glBufferStorage(GL_SHADER_STORAGE_BUFFER, 4 * sizeof(float) * maxPartsAmount, velocs,
		GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT);
	velocs = (float*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, 4 * sizeof(float) * maxPartsAmount,
		GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT);

	glGenBuffers(1, &colSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, colSSbo);
	glBufferStorage(GL_SHADER_STORAGE_BUFFER, 4 * sizeof(float) * maxPartsAmount, colors,
		GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT);
	colors = (float*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, 4 * sizeof(float) * maxPartsAmount,
		GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT);

	glGenBuffers(1, &massSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, massSSbo);
	glBufferStorage(GL_SHADER_STORAGE_BUFFER, sizeof(float) * maxPartsAmount, masses,
		GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT);
	masses = (float*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, sizeof(float) * maxPartsAmount,
		GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT);

	glGenBuffers(1, &lifeTimesSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, lifeTimesSSbo);
	glBufferStorage(GL_SHADER_STORAGE_BUFFER, sizeof(float) * maxPartsAmount, lifeTimes,
		GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT);

	lifeTimes = (float*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, sizeof(float) * maxPartsAmount,
		GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT);


	glGenBuffers(1, &iniLifeTimesSSbo);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, iniLifeTimesSSbo);
	glBufferStorage(GL_SHADER_STORAGE_BUFFER, sizeof(float) * maxPartsAmount, iniLifeTimes,
		GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT);

	iniLifeTimes = (float*)glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, sizeof(float) * maxPartsAmount,
		GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT);

	//Se activan para ser indexados dentro del compute shader (excepto los attractors, que lo hacen en su funci�n)
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, posSSbo);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, velSSbo);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, colSSbo);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, massSSbo);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 7, lifeTimesSSbo);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 8, iniLifeTimesSSbo);

	//Subida de uniforms

	//Compute program
	extForcesU = glGetUniformLocation(computeProgramID, "externalForces");
	glUniform3fv(extForcesU, 1, &externalForces[0]);

	deltatTU = glGetUniformLocation(computeProgramID, "dt");
	glUniform1f(deltatTU, deltaTime);

	sphereDataU = glGetUniformLocation(computeProgramID, "sphereData");
	glUniform4fv(sphereDataU, 1, &sphereData[0]);

	minCubeU = glGetUniformLocation(computeProgramID, "minCube");
	glUniform3fv(minCubeU, 1, &minBoxData[0]);

	maxCubeU = glGetUniformLocation(computeProgramID, "maxCube");
	glUniform3fv(maxCubeU, 1, &maxBoxData[0]);

	restitutionU = glGetUniformLocation(computeProgramID, "m_bounce");
	glUniform1f(restitutionU, restitution);

	//Graphic program
	sphereDataGraphU = glGetUniformLocation(graphicProgramID, "sphereData");
	glUniform4fv(sphereDataGraphU, 1, &sphereData[0]);

	minCubeGraphU = glGetUniformLocation(graphicProgramID, "minCube");
	glUniform3fv(minCubeGraphU, 1, &minBoxData[0]);

	maxCubeGraphU = glGetUniformLocation(graphicProgramID, "maxCube");
	glUniform3fv(maxCubeGraphU, 1, &maxBoxData[0]);

	//Binding buffers
	glGenVertexArrays(1, &pointsVAOHandle);
	glBindVertexArray(pointsVAOHandle);

	//Se utilizar los SSBO como VBO en el VAO
	glBindBuffer(GL_ARRAY_BUFFER, posSSbo);
	GLuint loc = glGetAttribLocation(graphicProgramID, "aPosition");
	glEnableVertexAttribArray(loc);
	glVertexAttribPointer(loc, 4, GL_FLOAT, GL_FALSE, 0, (char *)NULL + 0);

	glBindBuffer(GL_ARRAY_BUFFER, colSSbo);
	GLuint loc2 = glGetAttribLocation(graphicProgramID, "aColor");
	glEnableVertexAttribArray(loc2);
	glVertexAttribPointer(loc2, 4, GL_FLOAT, GL_FALSE, 0, (char *)NULL + 0);
	
	glBindBuffer(GL_ARRAY_BUFFER, lifeTimesSSbo);
	GLuint loc3 = glGetAttribLocation(graphicProgramID, "lifeTime");
	glEnableVertexAttribArray(loc3);
	glVertexAttribPointer(loc3, 1, GL_FLOAT, GL_FALSE, 0, (char *)NULL + 0);

	glBindBuffer(GL_ARRAY_BUFFER, iniLifeTimesSSbo);
	GLuint loc4 = glGetAttribLocation(graphicProgramID, "iniLifeTime");
	glEnableVertexAttribArray(loc4);
	glVertexAttribPointer(loc4, 1, GL_FLOAT, GL_FALSE, 0, (char *)NULL + 0);

	glBindVertexArray(0);
}


int iter = 0;

void ParticleSystem::Update(float deltaTime)
{
	/*
		0.- Actualizar datos de uniforms
		1.- Comprobar si el sistema de part�culas tiene nuevas part�culas "muertas" y actualizar numeraciones.
		2.- Actualizar emisores y sus potenciales nuevas part�culas generadas.
	*/
	//Compute program
	glUniform3fv(extForcesU, 1, &externalForces[0]);
	glUniform1f(deltatTU, deltaTime);
	glUniform4fv(sphereDataU, 1, &sphereData[0]);
	glUniform3fv(minCubeU, 1, &minBoxData[0]);
	glUniform3fv(maxCubeU, 1, &maxBoxData[0]);
	glUniform1f(restitutionU, restitution);
	glUniform1i(attractorNumberU, attractorNumber);

	//Graphic program
	glUniform4fv(sphereDataGraphU, 1, &sphereData[0]);
	glUniform3fv(minCubeGraphU, 1, &minBoxData[0]);
	glUniform3fv(maxCubeGraphU, 1, &maxBoxData[0]);

		for (size_t i = currentPartsAmount; i > 0; i--)
		{
			if (lifeTimes[i - 1] <= 0.0f) {
				RemoveParticle(i - 1);
			}
		}

	for (size_t i = 0; i < emitters.size(); i++)
		emitters[i].Update(deltaTime, this);
	iter++;
}

void ParticleSystem::DrawParticles()
{
	/*Aqu� dibujamos todas las part�culas debido a que es m�s �ptimo
		que estar ordenando las listas de �stas una y otra vez cuando
		alguna de ellas muere.
	*/
	//cout << currentPartsAmount << endl;
	glBindVertexArray(pointsVAOHandle);
	glDrawArrays(GL_POINTS, 0, currentPartsAmount);
	glBindVertexArray(0); 
}

void ParticleSystem::Destroy()
{
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, posSSbo);
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	delete[]points;

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, colSSbo);
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	delete[]colors;

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, velSSbo);
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	delete[]velocs;

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, massSSbo);
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	delete[]masses;

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, lifeTimesSSbo);
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	delete[]lifeTimes;

	glBindBuffer(GL_SHADER_STORAGE_BUFFER, iniLifeTimesSSbo);
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
	delete[]iniLifeTimes;
}
