#pragma once
#include <glm/glm.hpp>
#include <GL/glew.h>
#include "Emitter.h"
#include <vector>
using namespace std;
using namespace glm;

//In this design attractors are only points in space

class ParticleSystem
{
public:

	int maxPartsAmount;
	int currentPartsAmount;

	ParticleSystem(){}

	ParticleSystem(unsigned int maxPartsAmount, unsigned int partsAmount,
		float deltaTime, vec4 externalForces, vec4 sphereData,
		vec3 minBoxData, vec3 maxBoxData,
		float restitution, float mass,
		GLuint graphicProgramID,
		GLuint computeProgramID
	);

	void Update(float deltaTime);
	void DrawParticles();
	void AddParticle(float mass, vec4 position, vec4 color, vec4 veloc, float startingLifeTime);
	
	void InitializeEmitters(unsigned int emitterNumber, float pps, float strenght);
	void InitializeEmitters(unsigned int emitterNumber);
	void InitializeAttractors(unsigned int attractorNumber, GLuint computeProgramID);
	void BindAttractors(GLuint computeProgramID);

	void AddEmitter(vec3 position, vec3 direction, float pps, float strenght);
	void AddAttractor(unsigned int attractorId, vec4 position, float strength);

	void Destroy();

private:
	void InitializeParticles(unsigned int partsAmount);
	void RemoveParticle(unsigned int index);
	GLuint pointsVAOHandle;

	float deltaTime;
	float restitution;
	vec4 externalForces;
	vec4 sphereData;
	vec3 minBoxData;
	vec3 maxBoxData;
	float mass;

	float* points;
	float* colors;
	float* velocs;
	float* masses;
	float* lifeTimes;
	
	float* iniLifeTimes;

	vector<Emitter> emitters;

	float* attractors;
	float* attractorsStrengths;
	unsigned int attractorNumber;

	GLuint posSSbo;
	GLuint velSSbo;
	GLuint colSSbo;
	GLuint massSSbo;
	GLuint attSSbo;
	GLuint attStrSSbo;
	GLuint lifeTimesSSbo;
	GLuint iniLifeTimesSSbo;

	GLuint extForcesU;
	GLuint deltatTU;
	GLuint sphereDataU;
	GLuint minCubeU;
	GLuint maxCubeU;
	GLuint restitutionU;
	GLuint attractorNumberU;

	GLuint sphereDataGraphU;
	GLuint minCubeGraphU;
	GLuint maxCubeGraphU;

	GLuint textureU;
};