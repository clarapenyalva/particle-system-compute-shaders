#pragma once
#include <glm/glm.hpp>

using namespace std;
using namespace glm;

inline float ranf(float min = 0.0f, float max = 1.0f)
{
	return ((max - min) * rand() / RAND_MAX + min);
}

inline vec3 ranv3(float min = -1.0f, float max = 1.0f)
{
	return vec3(ranf(min, max), ranf(min, max), ranf(min, max));
}

inline vec4 ranv4(float lastValue, float min = 0.0f, float max = 1.0f)
{
	return vec4(ranf(min, max), ranf(min, max), ranf(min, max), lastValue);
}

unsigned char *loadTexture(const char* fileName, unsigned int &w, unsigned int &h);