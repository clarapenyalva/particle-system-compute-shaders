#include <GL/glew.h>
#include <GL/glut.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "tga.h"
#include "ParticleSystem.h"
#include "Utils.h"
#include <chrono>

const float PI = 3.141593f;
glm::vec3 cameraPos(0, 0, 10);
glm::vec3 cameraRot(0, 0, 0);
float radius = glm::length(cameraPos);
glm::vec2 prevV(0, 0);
glm::vec2 rotV(0, 0);

glm::vec3 rightV(1.0f, 0.0f, 0.0f),
upV(0.0f, 1.0f, 0.0f),
forwardV(0.0f, 0.0f, -1.0f);

const int NUM_PARTICLES = 0;
const int WORK_GROUP_SIZE = 256;
const int MAX_PARTICLES = WORK_GROUP_SIZE * 8;

const int SMOKE = 0;
const int WATER = 1;
const int BOUNCYBALLS = 2;
const int BOUNCYBALLS_BLOCK = 3;
const int SMOKE_WINDY = 4;
int scene = 1; //CHANGE TO VIEW ANOTHER SCENE

void initCube();
void drawCube();

void loadSource(GLuint &shaderID, std::string name);
void printCompileInfoLog(GLuint shadID);
void printLinkInfoLog(GLuint programID);
void validateProgram(GLuint programID);
void drawOrbitalCamera();

bool init();
void display();
void resize(int, int);
void idle();
void keyboard(unsigned char, int, int);
void specialKeyboard(int, int, int);
void mouse(int, int, int, int);
void mouseMotion(int, int);
void onDestroy();

int LoadTex(string);

bool fullscreen = false;
bool mouseDown = false;
bool animation = false;
int lighting = 0;

float xrot = 0.0f;
float yrot = 0.0f;
float xdiff = 0.0f;
float ydiff = 0.0f;

int g_Width = 500;                          // Ancho inicial de la ventana
int g_Height = 500;                         // Altura incial de la ventana

GLuint cubeVAOHandle, pointsVAOHandle;
GLuint graphicProgramID, computeProgramID;
GLuint mvpmU, mvmU, pmU;
GLuint spriteSphereTexU, spriteSmokeTexU;
GLuint textSphereId, textSmokeId;
GLuint backgroundColorU;
GLuint sizeParticleU;

vec4 backgroundColor;
float sizeParticle;

chrono::time_point<chrono::steady_clock> deltaTimeChecker;

ParticleSystem partSys;

// cubo ///////////////////////////////////////////////////////////////////////
//    v6----- v5
//   /|      /|
//  v1------v0|
//  | |     | |
//  | |v7---|-|v4
//  |/      |/
//  v2------v3

// Coordenadas del vertex array  =====================================
// Un cubo tiene 6 lados y cada lado tiene 2 triangles, por tanto, un cubo
// tiene 36 v�rtices (6 lados * 2 trian * 3 vertices = 36 vertices). Y cada
// vertice tiene 4 components (x,y,z) de reales, por tanto, el tama�o del vertex
// array es 144 floats (36 * 4 = 144).
GLfloat vertices1[] = { 1, 1, 1, 1,  -1, 1, 1, 1,  -1,-1, 1, 1,   1,-1, 1, 1,    // v0-v1-v2-v3 (front)

						1, 1, 1, 1,   1,-1, 1, 1,   1,-1,-1, 1,   1, 1,-1, 1,    // v0-v3-v4-v5 (right)

						1, 1, 1, 1,   1, 1,-1, 1,  -1, 1,-1, 1,  -1, 1, 1, 1,    // v0-v5-v6-v1 (top)

					   -1, 1, 1, 1,  -1, 1,-1, 1,  -1,-1,-1, 1,  -1,-1, 1, 1,    // v1-v6-v7-v2 (left)

					   -1,-1,-1, 1,   1,-1,-1, 1,   1,-1, 1, 1,  -1,-1, 1, 1,    // v7-v4-v3-v2 (bottom)

						1,-1,-1, 1,  -1,-1,-1, 1,  -1, 1,-1, 1,   1, 1,-1, 1 };  // v4-v7-v6-v5 (back)

GLfloat colors1[] = { 0, 0, 1, 1,   0, 0, 1, 1,   0, 0, 1, 1,   0, 0, 1, 1,   // v0-v1-v2-v3 (front)

						1, 0, 0, 1,   1, 0, 0, 1,   1, 0, 0, 1,   1, 0, 0, 1,   // v0-v3-v4-v5 (right)

						0, 1, 0, 1,   0, 1, 0, 1,   0, 1, 0, 1,   0, 1, 0, 1,   // v0-v5-v6-v1 (top)

						1, 0, 0, 1,   1, 0, 0, 1,   1, 0, 0, 1,   1, 0, 0, 1,   // v1-v6-v7-v2 (left)

						0, 1, 0, 1,   0, 1, 0, 1,   0, 1, 0, 1,   0, 1, 0, 1,   // v7-v4-v3-v2 (bottom)

						0, 0, 1, 1,   0, 0, 1, 1,   0, 0, 1, 1,   0, 0, 1, 1 }; // v4-v7-v6-v5 (back)


// BEGIN: Carga shaders ////////////////////////////////////////////////////////////////////////////////////////////

void loadSource(GLuint &shaderID, std::string name)
{
	std::ifstream f(name.c_str());
	if (!f.is_open())
	{
		std::cerr << "File not found " << name.c_str() << std::endl;
		system("pause");
		exit(EXIT_FAILURE);
	}

	// now read in the data
	std::string *source;
	source = new std::string(std::istreambuf_iterator<char>(f),
		std::istreambuf_iterator<char>());
	f.close();

	// add a null to the string
	*source += "\0";
	const GLchar * data = source->c_str();
	glShaderSource(shaderID, 1, &data, NULL);
	delete source;
}

void printCompileInfoLog(GLuint shadID)
{
	GLint compiled;
	glGetShaderiv(shadID, GL_COMPILE_STATUS, &compiled);
	if (compiled == GL_FALSE)
	{
		GLint infoLength = 0;
		glGetShaderiv(shadID, GL_INFO_LOG_LENGTH, &infoLength);

		GLchar *infoLog = new GLchar[infoLength];
		GLint chsWritten = 0;
		glGetShaderInfoLog(shadID, infoLength, &chsWritten, infoLog);

		std::cerr << "Shader compiling failed:" << infoLog << std::endl;
		system("pause");
		delete[] infoLog;

		exit(EXIT_FAILURE);
	}
}

void printLinkInfoLog(GLuint programID)
{
	GLint linked;
	glGetProgramiv(programID, GL_LINK_STATUS, &linked);
	if (!linked)
	{
		GLint infoLength = 0;
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLength);

		GLchar *infoLog = new GLchar[infoLength];
		GLint chsWritten = 0;
		glGetProgramInfoLog(programID, infoLength, &chsWritten, infoLog);

		std::cerr << "Shader linking failed:" << infoLog << std::endl;
		system("pause");
		delete[] infoLog;

		exit(EXIT_FAILURE);
	}
}

void validateProgram(GLuint programID)
{
	GLint status;
	glValidateProgram(programID);
	glGetProgramiv(programID, GL_VALIDATE_STATUS, &status);

	if (status == GL_FALSE)
	{
		GLint infoLength = 0;
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLength);

		if (infoLength > 0)
		{
			GLchar *infoLog = new GLchar[infoLength];
			GLint chsWritten = 0;
			glGetProgramInfoLog(programID, infoLength, &chsWritten, infoLog);
			std::cerr << "Program validating failed:" << infoLog << std::endl;
			system("pause");
			delete[] infoLog;

			exit(EXIT_FAILURE);
		}
	}
}

// END:   Carga shaders ////////////////////////////////////////////////////////////////////////////////////////////

// BEGIN: Inicializa primitivas ////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// Init Cube
///////////////////////////////////////////////////////////////////////////////
void initCube()
{
	GLuint vboHandle;

	glGenVertexArrays(1, &cubeVAOHandle);
	glBindVertexArray(cubeVAOHandle);

	glGenBuffers(1, &vboHandle);
	glBindBuffer(GL_ARRAY_BUFFER, vboHandle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices1) + sizeof(colors1), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices1), vertices1);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices1), sizeof(colors1), colors1);

	GLuint loc = glGetAttribLocation(graphicProgramID, "aPosition");
	glEnableVertexAttribArray(loc);
	glVertexAttribPointer(loc, 4, GL_FLOAT, GL_FALSE, 0, (char *)NULL + 0);
	GLuint loc2 = glGetAttribLocation(graphicProgramID, "aColor");
	glEnableVertexAttribArray(loc2);
	glVertexAttribPointer(loc2, 4, GL_FLOAT, GL_FALSE, 0, (char *)NULL + sizeof(vertices1));

	glBindVertexArray(0);
}

// END: Inicializa primitivas ////////////////////////////////////////////////////////////////////////////////////

// BEGIN: Funciones de dibujo ////////////////////////////////////////////////////////////////////////////////////

void drawCube()
{
	glBindVertexArray(cubeVAOHandle);
	glDrawArrays(GL_QUADS, 0, 24);
	glBindVertexArray(0);
}


// END: Funciones de dibujo ////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
	glutInit(&argc, argv);
	glutInitWindowPosition(50, 50);
	glutInitWindowSize(g_Width, g_Height);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutCreateWindow("Programa Ejemplo");
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		/* Problem: glewInit failed, something is seriously wrong. */
		std::cerr << "Error: " << glewGetErrorString(err) << std::endl;
		system("pause");
		exit(-1);
	}

	init();

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(specialKeyboard);
	glutMouseFunc(mouse);
	glutMotionFunc(mouseMotion);
	glutReshapeFunc(resize);
	glutIdleFunc(idle);

	deltaTimeChecker = chrono::high_resolution_clock::now();

	glutMainLoop();

	onDestroy();

	return EXIT_SUCCESS;
}

void onDestroy()
{
	//destroy particle systems
	partSys.Destroy();
}

bool init()
{
	backgroundColor = vec4(1.0f, 1.0f, 1.0f, 0.0f);

	srand(time(NULL));

	glClearColor(backgroundColor.x, backgroundColor.y, backgroundColor.z, backgroundColor.w);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glDepthFunc(GL_LESS);
	glClearDepth(1.0f);

	glShadeModel(GL_SMOOTH);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Graphic shaders program
	graphicProgramID = glCreateProgram();

	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	loadSource(vertexShaderID, "Shaders/particles.vert");
	std::cout << "Compiling Vertex Shader" << std::endl;
	glCompileShader(vertexShaderID);
	printCompileInfoLog(vertexShaderID);
	glAttachShader(graphicProgramID, vertexShaderID);

	GLuint geometryShaderID = glCreateShader(GL_GEOMETRY_SHADER);
	loadSource(geometryShaderID, "Shaders/particles.geom");
	std::cout << "Compiling Geometry Shader" << std::endl;
	glCompileShader(geometryShaderID);
	printCompileInfoLog(geometryShaderID);
	glAttachShader(graphicProgramID, geometryShaderID);

	//Load sphere texture
	TGAFILE tgaImage;
	glGenTextures(1, &textSphereId);
	if (LoadTGAFile("img/white_sphere.tga", &tgaImage))
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, textSphereId);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tgaImage.imageWidth, tgaImage.imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, tgaImage.imageData);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}

	//Load smoke texture
	textSmokeId = LoadTex("img/particle_smoke.png");
	glActiveTexture(GL_TEXTURE0 + 1);
	glBindTexture(GL_TEXTURE_2D, textSmokeId);
	glUniform1i(textSmokeId, 1);

	computeProgramID = glCreateProgram();

	GLuint computeShaderID = glCreateShader(GL_COMPUTE_SHADER);

	if (scene == SMOKE)
	{
		loadSource(computeShaderID, "Shaders/complexComp.glsl");
		std::cout << "Compiling Compute Shader" << std::endl;
		glCompileShader(computeShaderID);
		printCompileInfoLog(computeShaderID);
		glAttachShader(computeProgramID, computeShaderID);

		glLinkProgram(computeProgramID);
		printLinkInfoLog(computeProgramID);
		validateProgram(computeProgramID);

		sizeParticle = 0.5f;

		GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
		loadSource(fragmentShaderID, "Shaders/smoke.frag");
		std::cout << "Compiling Fragment Shader" << std::endl;
		glCompileShader(fragmentShaderID);
		printCompileInfoLog(fragmentShaderID);
		glAttachShader(graphicProgramID, fragmentShaderID);

		glLinkProgram(graphicProgramID);
		printLinkInfoLog(graphicProgramID);
		validateProgram(graphicProgramID);

		partSys = ParticleSystem(
			MAX_PARTICLES, NUM_PARTICLES,
			0.0166f, //Deltatime (initial)
			vec4(0.0f, 1.0f, 0.0f, 0.0f), //Total External Forces
			vec4(0.0f, -1.0f, 0.0f, 0.5f), //Sphere
			vec3(-1.0f, -1.0f, -1.0f), //Min corner cube
			vec3(1.0f, 1.0f, 1.0f),  //Max corner cube
			0.8f, //Restitution
			4.0f,
			graphicProgramID,
			computeProgramID
		);

		partSys.InitializeEmitters(3);
		partSys.AddEmitter(vec3(-0.9f, -0.9f, 0.9f), vec3(0.0f, 1.0f, 0.0f), 3, 0.0f);
		partSys.AddEmitter(vec3(0.9f, -0.9f, 0.9f), vec3(0.0f, 1.0f, 0.0f), 3, 0.0f);
		partSys.AddEmitter(vec3(0.0f, -0.9f, -0.9f), vec3(0.0f, 1.0f, 0.0f), 6, 0.5f);

		partSys.InitializeAttractors(1, computeProgramID);
		partSys.AddAttractor(0, vec4(0.0f, 3.0f, 0.0f, 1.0f), 0.3f);
		partSys.BindAttractors(computeProgramID);
	}
	else if (scene == WATER)
	{
		loadSource(computeShaderID, "Shaders/complexComp.glsl");
		std::cout << "Compiling Compute Shader" << std::endl;
		glCompileShader(computeShaderID);
		printCompileInfoLog(computeShaderID);
		glAttachShader(computeProgramID, computeShaderID);

		glLinkProgram(computeProgramID);
		printLinkInfoLog(computeProgramID);
		validateProgram(computeProgramID);


		sizeParticle = 0.1f;

		GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
		loadSource(fragmentShaderID, "Shaders/water.frag");
		std::cout << "Compiling Fragment Shader" << std::endl;
		glCompileShader(fragmentShaderID);
		printCompileInfoLog(fragmentShaderID);
		glAttachShader(graphicProgramID, fragmentShaderID);

		glLinkProgram(graphicProgramID);
		printLinkInfoLog(graphicProgramID);
		validateProgram(graphicProgramID);

		partSys = ParticleSystem(
			MAX_PARTICLES, NUM_PARTICLES,
			0.0166f, //Deltatime (initial)
			vec4(0.0f, -9.8f, 0.0f, 0.0f), //Total External Forces
			vec4(0.0f, -1.0f, 0.0f, 0.5f), //Sphere
			vec3(-1.0f, -1.0f, -1.0f), //Min corner cube
			vec3(1.0f, 1.0f, 1.0f),  //Max corner cube
			0.3f, //Restitution
			.6f,
			graphicProgramID,
			computeProgramID
		);

		partSys.InitializeEmitters(1);
		partSys.AddEmitter(vec3(0.0f, -.9f, 0.0f), vec3(0.0f, 1.0f, 0.0f), 100, 5.0f);

		partSys.InitializeAttractors(0, computeProgramID);
		//partSys.AddAttractor(0, vec4(0.0f, 3.0f, 0.0f, 1.0f), 0.3f);
		partSys.BindAttractors(computeProgramID);
	}
	else if (scene == BOUNCYBALLS)
	{
		loadSource(computeShaderID, "Shaders/particlesComp.glsl");
		std::cout << "Compiling Compute Shader" << std::endl;
		glCompileShader(computeShaderID);
		printCompileInfoLog(computeShaderID);
		glAttachShader(computeProgramID, computeShaderID);

		glLinkProgram(computeProgramID);
		printLinkInfoLog(computeProgramID);
		validateProgram(computeProgramID);


		sizeParticle = 0.1f;

		GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
		loadSource(fragmentShaderID, "Shaders/balls.frag");
		std::cout << "Compiling Fragment Shader" << std::endl;
		glCompileShader(fragmentShaderID);
		printCompileInfoLog(fragmentShaderID);
		glAttachShader(graphicProgramID, fragmentShaderID);

		glLinkProgram(graphicProgramID);
		printLinkInfoLog(graphicProgramID);
		validateProgram(graphicProgramID);
		partSys = ParticleSystem(
			MAX_PARTICLES, NUM_PARTICLES,
			0.0166f, //Deltatime (initial)
			vec4(0.0f, -9.8f, 0.0f, 0.0f), //Total External Forces
			vec4(0.0f, -1.0f, 0.0f, 0.5f), //Sphere
			vec3(-1.0f, -1.0f, -1.0f), //Min corner cube
			vec3(1.0f, 1.0f, 1.0f),  //Max corner cube
			1.5f, //Restitution
			.8f,
			graphicProgramID,
			computeProgramID
		);

		partSys.InitializeEmitters(4);
		partSys.AddEmitter(vec3(0.9f, 0.9f, 0.9f), vec3(-1.0f, -1.5f, -1.0f), 20, 10.0f);
		partSys.AddEmitter(vec3(-0.9f, 0.9f, 0.9f), vec3(1.0f, -1.5f, -1.0f), 20, 10.0f);
		partSys.AddEmitter(vec3(0.9f, 0.9f, -0.9f), vec3(-1.0f, -1.5f, 1.0f), 20, 10.0f);
		partSys.AddEmitter(vec3(-0.9f, 0.9f, -0.9f), vec3(1.0f, -1.5f, 1.0f), 20, 10.0f);

		partSys.InitializeAttractors(0, computeProgramID);
		//partSys.AddAttractor(0, vec4(0.0f, 3.0f, 0.0f, 1.0f), 0.3f);
		partSys.BindAttractors(computeProgramID);
	}
	else if (scene == BOUNCYBALLS_BLOCK)
	{
		loadSource(computeShaderID, "Shaders/particlesComp.glsl");
		std::cout << "Compiling Compute Shader" << std::endl;
		glCompileShader(computeShaderID);
		printCompileInfoLog(computeShaderID);
		glAttachShader(computeProgramID, computeShaderID);

		glLinkProgram(computeProgramID);
		printLinkInfoLog(computeProgramID);
		validateProgram(computeProgramID);


		sizeParticle = 0.1f;

		GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
		loadSource(fragmentShaderID, "Shaders/balls.frag");
		std::cout << "Compiling Fragment Shader" << std::endl;
		glCompileShader(fragmentShaderID);
		printCompileInfoLog(fragmentShaderID);
		glAttachShader(graphicProgramID, fragmentShaderID);

		glLinkProgram(graphicProgramID);
		printLinkInfoLog(graphicProgramID);
		validateProgram(graphicProgramID);
		partSys = ParticleSystem(
			MAX_PARTICLES, 1024,
			0.0166f, //Deltatime (initial)
			vec4(0.0f, -9.8f, 0.0f, 0.0f), //Total External Forces
			vec4(0.0f, -1.0f, 0.0f, 0.5f), //Sphere
			vec3(-1.0f, -1.0f, -1.0f), //Min corner cube
			vec3(1.0f, 1.0f, 1.0f),  //Max corner cube
			1.0f, //Restitution
			.8f, //size
			graphicProgramID,
			computeProgramID
		);

		partSys.InitializeEmitters(0);

		partSys.InitializeAttractors(0, computeProgramID);
		//partSys.AddAttractor(0, vec4(0.0f, 3.0f, 0.0f, 1.0f), 0.3f);
		partSys.BindAttractors(computeProgramID);
	}
	else if (scene == SMOKE_WINDY)
	{
		loadSource(computeShaderID, "Shaders/complexComp.glsl");
		std::cout << "Compiling Compute Shader" << std::endl;
		glCompileShader(computeShaderID);
		printCompileInfoLog(computeShaderID);
		glAttachShader(computeProgramID, computeShaderID);

		glLinkProgram(computeProgramID);
		printLinkInfoLog(computeProgramID);
		validateProgram(computeProgramID);

		sizeParticle = 0.5f;

		GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
		loadSource(fragmentShaderID, "Shaders/smoke.frag");
		std::cout << "Compiling Fragment Shader" << std::endl;
		glCompileShader(fragmentShaderID);
		printCompileInfoLog(fragmentShaderID);
		glAttachShader(graphicProgramID, fragmentShaderID);

		glLinkProgram(graphicProgramID);
		printLinkInfoLog(graphicProgramID);
		validateProgram(graphicProgramID);

		partSys = ParticleSystem(
			MAX_PARTICLES, NUM_PARTICLES,
			0.0166f, //Deltatime (initial)
			vec4(0.0f, 2.0f, 0.0f, 0.0f), //Total External Forces
			vec4(0.0f, -1.0f, 0.0f, 0.5f), //Sphere
			vec3(-1.0f, -1.0f, -1.0f), //Min corner cube
			vec3(1.0f, 1.0f, 1.0f),  //Max corner cube
			0.f, //Restitution
			4.0f,
			graphicProgramID,
			computeProgramID
		);

		partSys.InitializeEmitters(2);
		partSys.AddEmitter(vec3(-0.9f, -0.9f, 0.9f), vec3(0.0f, 1.0f, 0.0f), 10, 0.0f);
		partSys.AddEmitter(vec3(-0.9f, -0.9f, -0.9f), vec3(0.0f, 1.0f, 0.0f), 10, 0.0f);

		partSys.InitializeAttractors(2, computeProgramID);
		partSys.AddAttractor(0, vec4(0.9f, .9f, -0.9f, 1.0f), 5.f);
		partSys.AddAttractor(1, vec4(0.9f, .9f, 0.9f, 1.0f), 05.f);
		partSys.BindAttractors(computeProgramID);
	}

	mvpmU = glGetUniformLocation(graphicProgramID, "uModelViewProjMatrix");
	mvmU = glGetUniformLocation(graphicProgramID, "uModelViewMatrix");
	pmU = glGetUniformLocation(graphicProgramID, "uProjectionMatrix");
	spriteSphereTexU = glGetUniformLocation(graphicProgramID, "uSpriteSphereTex");
	spriteSmokeTexU = glGetUniformLocation(graphicProgramID, "uSpriteSmokeTex");
	backgroundColorU = glGetUniformLocation(graphicProgramID, "uBackgroundColor");
	sizeParticleU = glGetUniformLocation(graphicProgramID, "uSizeParticle");

	drawOrbitalCamera();

	return true;
}

glm::mat4 View;

void display()
{
	chrono::time_point<chrono::steady_clock> newDTChecker = chrono::high_resolution_clock::now();
	double deltaTime = std::chrono::duration_cast<std::chrono::nanoseconds>(newDTChecker - deltaTimeChecker).count() / 1e9;
	deltaTimeChecker = newDTChecker;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 Projection = glm::perspective(45.0f, 1.0f * g_Width / g_Height, 1.0f, 100.0f);

	//glm::vec3 cameraPos = glm::vec3(9.0f * cos(yrot / 100), 2.0f * sin(xrot / 100) + 2.0f, 9.0f * sin(yrot / 100) * cos(xrot / 100));
	//View = glm::lookAt(cameraPos, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	//glm::vec3 cameraPos = glm::vec3(0.0f, 10.0f, 0.0f);
	//glm::mat4 View = glm::lookAt(cameraPos, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));

	/*glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 10.0f);
	glm::mat4 View = glm::lookAt(cameraPos, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));*/

	glm::mat4 ModelCube = glm::translate(glm::scale(glm::mat4(1.0f), glm::vec3(2.0f, 2.0f, 2.0f)), glm::vec3(0.0f, 0.0f, 0.0f));

	glm::mat4 mvp; // Model-view-projection matrix
	glm::mat4 mv;  // Model-view matrix


	glUseProgram(computeProgramID);

	//Lanza su ejecuci�n, indicando el tama�o del dispatch
	glDispatchCompute(MAX_PARTICLES / WORK_GROUP_SIZE, 1, 1);

	partSys.Update(deltaTime);

	//Coloca un barrera de memoria antes de activar el objeto programa con los shaders gr�ficos
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	glUseProgram(graphicProgramID);

	// Dibuja Puntos
	mvp = Projection * View * ModelCube;
	mv = View * ModelCube;
	glUniformMatrix4fv(mvpmU, 1, GL_FALSE, &mvp[0][0]);
	glUniformMatrix4fv(mvmU, 1, GL_FALSE, &mv[0][0]);
	glUniformMatrix4fv(pmU, 1, GL_FALSE, &Projection[0][0]);
	glUniform4fv(backgroundColorU, 1, &backgroundColor[0]);
	glUniform1f(sizeParticleU, sizeParticle);

	drawCube();

	if (spriteSphereTexU != -1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, textSphereId);
		glUniform1i(spriteSphereTexU, 0);
	}

	if (spriteSmokeTexU != -1)
	{
		glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, textSmokeId);
		glUniform1i(spriteSmokeTexU, 1);
	}

	partSys.DrawParticles();

	glUseProgram(0);

	glutSwapBuffers();
}

void resize(int w, int h)
{
	g_Width = w;
	g_Height = h;
	glViewport(0, 0, g_Width, g_Height);
}

void idle()
{

	glutPostRedisplay();
}


void drawOrbitalCamera()
{
	glm::mat4 cameraTransf(1.0);
	cameraTransf = glm::rotate(cameraTransf, -rotV.x * 180.0f / PI, upV);
	cameraTransf = glm::rotate(cameraTransf, -rotV.y* 180.0f / PI, rightV);
	cameraTransf = glm::translate(cameraTransf, glm::vec3(0, 0, radius));

	glm::mat4 view = glm::inverse(cameraTransf);
	View = view;
}


void keyboard(unsigned char key, int x, int y)
{
	float scrollSpeed = 2;

	switch (key)
	{
	case 'w':
		radius -= scrollSpeed;
		break;
	case 's':
		radius += scrollSpeed;
		break;
	default:
		break;
	}

	drawOrbitalCamera();
}

void specialKeyboard(int key, int x, int y)
{
	if (key == GLUT_KEY_F1)
	{
		fullscreen = !fullscreen;

		if (fullscreen)
			glutFullScreen();
		else
		{
			glutReshapeWindow(g_Width, g_Height);
			glutPositionWindow(50, 50);
		}
	}
}

void mouse(int button, int state, int x, int y)
{	
	if (button == 0 && state == 0)
	{
		//Se acaba de apretar el bot�n izquierdo
		prevV = glm::vec2(x, y);
	}
}

vec3 ClampVector(vec3 vec, float min, float max)
{
	return vec3(clamp(vec.x, min, max), clamp(vec.y, min, max), clamp(vec.z, min, max));
}

void mouseMotion(int x, int y)
{

	static glm::vec2 sensitivity(0.03, 0.03);

	glm::vec2 displacement = glm::vec2(x, y) - prevV;

	rotV += displacement * sensitivity;


	rotV = glm::vec2(rotV.x, clamp(rotV.y, -PI * 0.5f, PI*0.5f));

	drawOrbitalCamera();

	prevV = glm::vec2(x, y);
	std::cout << radius << std::endl;
}

int LoadTex(string fileName)
{
	if (fileName.empty()) return -1;

	unsigned char *map;
	unsigned int w, h;

	map = loadTexture(fileName.c_str(), w, h);
	if (!map)
	{
		std::cout << "Error cargando el fichero: "
			<< fileName.c_str() << std::endl;
		exit(-1);
	}

	unsigned int texId;
	glGenTextures(1, &texId);
	glBindTexture(GL_TEXTURE_2D, texId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA,
		GL_UNSIGNED_BYTE, (GLvoid*)map); //Reserva espacio para la textura2d activa y la sube. Es mutable.

	delete[] map;

	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		GL_LINEAR_MIPMAP_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);

	//https://stackoverflow.com/questions/18447881/opengl-anisotropic-filtering-support-contradictory-check-results
	//if (glewIsSupported("GL_EXT_texture_filter_anisotropic"))
	//{
	GLfloat fLargest;
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, fLargest);
	//}

	return texId;
}