#version 430

layout(std140, binding = 0) buffer Pos {
	vec4 Positions[];
};
layout(std140, binding = 2) buffer Vel {
	vec4 Velocities[];
};
layout(std140, binding = 3) buffer Col {
	vec4 Colors[];
};
layout(std140, binding = 4) buffer Mass { 
	vec4 Masses[];
};
layout(std140, binding = 5) buffer AttPos {
	vec4 AttPositions[];
};
layout(std140, binding = 6) buffer AttForce {
	float AttForces[];
};
layout(std140, binding = 7) buffer LifeTime {
	vec4 LifeTimes[];
};

//tama�o del grupo de trabajo local
layout(local_size_x = 256, local_size_y = 1, local_size_z = 1) in;

uniform vec3 externalForces;
uniform float dt;
uniform vec4 sphereData; //(pos, radius)
uniform vec3 minCube;
uniform vec3 maxCube;
uniform float m_bounce;
uniform int attractorNumber;

const vec3 maxPosCube = vec3(5.0f, 5.0f, 5.0f);
const vec3 minPosCube = vec3(-5.0f, -1.0f, -5.0f);

//Auxiliar functions
bool IsInsideSphere(vec3 p, vec4 sphere);
void ComputeCollision(vec3 norm, inout vec3 pos, inout vec3 newPos, inout vec3 newVel);
bool IsOutsideCube(vec3 p);
vec3 updateForces(float mass, vec3 pos);

void main()
{
	// id of the thread (particle)
	uint gid = gl_GlobalInvocationID.x;

	int index = int(ceil(gid/4));
	int subIndex = int(gid%4);
	
	//check that in this position there is a living particle
	float lifeTime = LifeTimes[index][subIndex];

	if (lifeTime > 0.0) {
		// read the buffers
		vec3 pos = Positions[gid].xyz;
		vec3 vel = Velocities[gid].xyz;
		float mass = Masses[index][subIndex];

		vec3 norm;
		vec3 newPos;
		vec3 newVel;
		vec3 force = updateForces(mass, pos);
		
		//Euler
		newVel = vel + (force / mass) * dt;
		newPos = pos + newVel * dt;  

		//Detect and manage collisions
		//if (IsInsideSphere(newPos, sphereData))
		//{
		//	norm = normalize(pos - sphereData.xyz);
		//	ComputeCollision(norm, pos, newPos, newVel);
		//}
		
		//if (IsOutsideCube(newPos))
		if (newPos.x < minPosCube.x || newPos.x > maxPosCube.x ||
		newPos.y < minPosCube.y || newPos.y > maxPosCube.y ||
		newPos.z < minPosCube.z || newPos.z > maxPosCube.z)
		{
			vec3 posInside = clamp(newPos, -1, 1);
			norm = normalize(newPos - posInside);
			ComputeCollision(norm, pos, newPos, newVel);	
		}

		// Update buffer values
		Positions[gid].xyz = newPos;
		Velocities[gid].xyz = newVel;
		LifeTimes[index][subIndex] -= dt;
	}
}


void ComputeCollision(vec3 norm, inout vec3 pos, inout vec3 newPos, inout vec3 newVel)
{
	
	newVel = reflect(newVel, norm) * m_bounce;
	newPos = pos + newVel * dt;
}

bool IsInsideSphere(vec3 p, vec4 sphere)
{
	float r = length(p - sphere.xyz);
	return (r < sphere.w);
}

bool IsOutsideCube(vec3 newPos)
{
	return (newPos.x < minCube.x || newPos.x > maxCube.x ||
		newPos.y < minCube.y || newPos.y > maxCube.y ||
		newPos.z < minCube.z || newPos.z > maxCube.z);
}


vec3 updateForces(float mass, vec3 pos) 
{
	vec3 force = vec3(0.0f);

	//attractors
	for (int i = 0; i < attractorNumber; i++)
	{
		vec3 r = AttPositions[i].xyz - pos.xyz;
		float rLength = length(r);
		force += AttForces[i] * mass / (rLength*rLength) * r;		
	}

	//external forces (gravity, wind, etc.)
	force += externalForces;

	return force;
}












