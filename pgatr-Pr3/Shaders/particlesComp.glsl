#version 430

layout(std140, binding = 0) buffer Pos {
	vec4 Positions[];
};
layout(std140, binding = 2) buffer Vel {
	vec4 Velocities[];
};

layout(std140, binding = 7) buffer LifeTime {
	vec4 LifeTimes[];
};
layout(std140, binding = 3) buffer Col {
	vec4 Colors[];
};

// Tarea por hacer: definir el tama�o del grupo de trabajo local.
layout(local_size_x = 256, local_size_y = 1, local_size_z = 1) in;

const float dt = 0.005;
const float tamCube = 1.0; // El cubo que engloba las particulas x,y,z = [-tamCube, tamCube]
const vec3 g = vec3(0.0, -9.8, 0.0);

const vec4 S = vec4(0., -1.0, 0., .5); // x, y, z, r

bool IsInsideSphere(vec3 p, vec4 sphere)
{
	float r = length(p - sphere.xyz);
	return (r < sphere.w);
}

void main()
{
	uint gid = gl_GlobalInvocationID.x;

	int index = int(ceil(gid/4));
	int subIndex = int(gid%4);
	if(LifeTimes[index][subIndex] <= 0.0)
	return;

	// Tarea por hacer: obtener el id del hilo (part�cula)

	// leer su posici�n y velocidad de los buffers
	vec3 p = Positions[gid].xyz;
	vec3 v = Velocities[gid].xyz;

	// calcular la nueva posici�n y velocidad (si choca con las paredes del cubo la part�cula cambia de direcci�n)
	vec3 vp = v + g * dt;
	vec3 pp = p + v * dt + 0.5 * dt * dt * g;
	vec3 n;

	if (IsInsideSphere(pp, S))
	{
		n = normalize(p - S.xyz);
		vp = reflect(v, n) * 0.9;

		pp = p + vp * dt + 0.5 * dt * dt * g;
	}

	if (pp.x < -tamCube || pp.x > tamCube ||
	pp.y < -tamCube || pp.y > tamCube ||
	pp.z < -tamCube || pp.z > tamCube)
	{
		vec3 posInside = clamp(pp, -1, 1);
		n = normalize(pp - posInside);
		vp = reflect(v, n)*0.9;
		pp = p + vp * dt + 0.5 * dt * dt * g;
	}

	// Actualizar los valores de los buffers.
	Positions[gid].xyz = pp;
	Velocities[gid].xyz = vp;
	//LifeTimes[index][subIndex] -= dt;
}


