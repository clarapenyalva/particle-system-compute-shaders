#version 430

//in vec4 vColor;

// Tarea por hacer: recoger (como datos de entrada) los datos de salida del geometry shader.
in vec2 gTexCoord;
in vec4 gColor;

flat in float gLifeTime;
flat in float gIniLifeTime;

uniform sampler2D uSpriteSphereTex;

uniform vec4 uBackgroundColor;

out vec4 fFragColor;

const float minLife = 0.0f;

void main()
{
	if ( length(gTexCoord - 0.5) > 0.38 ) discard;

    vec4 color = texture(uSpriteSphereTex, gTexCoord);
	color *= gColor;
	//Color interpolated
	vec3 colorInterp = (vec3(gLifeTime) - vec3(minLife)) / (vec3(gIniLifeTime) - vec3(minLife)) 
	* (color.xyz - uBackgroundColor.xyz) + uBackgroundColor.xyz;
	fFragColor = vec4(colorInterp, 1.0f);
}