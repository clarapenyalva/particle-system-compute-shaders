#version 430  

in vec4 aPosition;
in vec4 aColor;
in float lifeTime;
in float iniLifeTime;

uniform mat4 uModelViewProjMatrix;
uniform mat4 uModelViewMatrix;

out vec4 vColor;
flat out float vLifeTime;
flat out float vIniLifeTime;

void main()
{
	vColor = aColor;
	vLifeTime = lifeTime;
	vIniLifeTime = iniLifeTime;

	gl_Position = uModelViewMatrix * aPosition;

}