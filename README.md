# Particle system with Compute Shaders

* Third practice assignment from Graphics Processors and Real-Time Applications. Master's Degree in Computer Graphics, Rey Juan Carlos University (Madrid).

* Developed using C++, OpenGL and GLSL.

Tasks performed:
* Vertex, fragment and geometry shaders.
* Basic particle system with compute shaders.
* Particle emitters. Adding life time to particles.
* Particle attractors. 
* Symplectic Euler physical integration of the particle system.
* Impostor method to visualize the particles using a texture.

(you can view the document "MemoriaP3-PGATR.pdf")

[Developed in 2019]

## Credits
* Clara Peñalva Carbonell [[webpage](https://clarapenyalva.com)] [[GitLab profile](https://gitlab.com/clarapenyalva)]
* José María Pizana García  [[webpage](https://jmpizana.com)] [[GitLab profile](https://gitlab.com/jmpizanagarcia)]